﻿using UnityEngine;
using System.Collections;

public class Ghosts : MonoBehaviour {

    public float speed = 0.4f;
    private Vector2 destination;
    private Vector2 direction;
    private Vector2 nextDirection;

    void Awake()
    {
        destination = Vector2.zero;
        direction = Vector2.zero;
        nextDirection = Vector2.zero;
    }

	void Start () {
        direction = Vector2.right;
        destination = transform.position;
	
	}

    void FixedUpdate()
    {
        Vector2 p = Vector2.MoveTowards(transform.position, destination, speed);
        GetComponent<Rigidbody2D>().MovePosition(p);

        if (Vector2.Distance(destination, transform.position) < 0.00001f)
        {
            nextDirection = Vector2.zero;

            if (Valid(Vector2.right)) {
                nextDirection = Vector2.right;
            }
            else if (Valid(Vector2.down)) {
                nextDirection = Vector2.down;
            }
            else if (Valid(Vector2.left))
            {
                nextDirection = Vector2.left;
            }
            else if (Valid(Vector2.up))
            {
                nextDirection = Vector2.up;
            }
            
        }
        if (Vector2.Distance(destination, transform.position) < 0.00001f)
        {
            if (Valid(nextDirection))
            {
                destination = (Vector2)transform.position + nextDirection;
                direction = nextDirection;
                //pacmanAnimator.SetFloat("DirX", nextDirection.x);
                //pacmanAnimator.SetFloat("DirY", nextDirection.y);
            }
            else
            {
                direction = direction;
                if (Valid(direction))
                {
                    destination = (Vector2)transform.position + direction;
                }
            }
        }
    }

    bool Valid(Vector2 direction)
    {
        Vector2 pos = transform.position;
        direction += new Vector2(direction.x * 0.45f, direction.y * 0.45f);
        RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
        if (hit.collider == null)
        {
            // Hit nothing
            return true;
        }
        else
        {
            // Hit something
            Debug.Log("LineCast hit: " + hit.transform.gameObject.name);
            if (hit.transform == this.transform)
            {
                return true;
            }
            else if (hit.transform.CompareTag("pacdot"))
            {
                return true;
            }
            else if (hit.transform.CompareTag("Item"))
            {
                return true;
            }
            else if (hit.transform.CompareTag("Player"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
