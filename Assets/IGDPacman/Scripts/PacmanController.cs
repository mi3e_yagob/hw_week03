﻿using UnityEngine;
using System.Collections;

public class PacmanController : MonoBehaviour {

    public float speed = 0.4f;
    private Vector2 destination;
    private Vector2 direction;
    private Vector2 nextDirection;
	private Animator pacmanAnimator;

    void Awake() {
        destination = Vector2.zero;
        direction = Vector2.zero;
        nextDirection = Vector2.zero;
		pacmanAnimator = GetComponent<Animator> ();
    }

    // Use this for initialization
    void Start () {
        direction = Vector2.right; // or direction = new Vector2(1, 0);
        destination = transform.position;
    }
	
    // Update is called once per frame
    void FixedUpdate () {
        // move closer to destination
        Vector2 p = Vector2.MoveTowards(transform.position, destination, speed);
        GetComponent<Rigidbody2D>().MovePosition(p);

        if (Input.GetAxis("Horizontal") > 0) {
            nextDirection = Vector2.right; // or nextDirection = new Vector2(1, 0);
        }
        else if (Input.GetAxis("Horizontal") < 0) {
            nextDirection = Vector2.left; // or nextDirection = new Vector2(-1, 0);
        }
        else if (Input.GetAxis("Vertical") > 0) {
            nextDirection = Vector2.up; // or nextDirection = new Vector2(0, 1);
        }
        else if (Input.GetAxis("Vertical") < 0) {
            nextDirection = Vector2.down; // or nextDirection = new Vector2(0, -1);
        }

        // if pacman is in the center of a tile
        if (Vector2.Distance(destination, transform.position) < 0.00001f) {
            if (Valid(nextDirection)) {
                destination = (Vector2)transform.position + nextDirection;
                direction = nextDirection;
				pacmanAnimator.SetFloat ("DirX", nextDirection.x);
				pacmanAnimator.SetFloat ("DirY", nextDirection.y);
            }
            else {
                direction = direction;
                if (Valid(direction)) {
                    destination = (Vector2)transform.position + direction;
                }
            }
        }
    }
    
    bool Valid(Vector2 direction) {
        Vector2 pos = transform.position;
        direction += new Vector2(direction.x * 0.45f, direction.y * 0.45f);
        RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
        if (hit.collider == null) {
            // Hit nothing
            return true;
        }
        else {
            // Hit something
            Debug.Log("LineCast hit: " + hit.transform.gameObject.name);
            if (hit.transform == this.transform) {
                return true;
            }
            //(hit.transform.name == "pacdot")
            else if (hit.transform.CompareTag("pacdot")) {
                return true;
            }
			else if (hit.transform.CompareTag("Item")) {
				return true;
			}
            else {
                return false;
            }
        }
    }
}
